# Firebase Case 00077741



## Problem

This project use a NPM package hosted on Gitlab with the name @firebase-00077741-cannot-use-external-repo/dependency. With the .npmrc settings, during `npm install` no problem occurs.

Occurs with firebase CLI 8.7.0

## How to

```bash
firebase use --add
cd functions && npm install
cd ..
firebase --debug --only functions deploy
```
